
CROSS_COMPILE = mipsel-openwrt-linux-
#CROSS_COMPILE = arm-linux-androideabi-
#CROSS_COMPILE = rsdk-linux-

OBJ = ngrok.o async.o parson.o utils.o

ifeq ($(CROSS_COMPILE), arm-linux-androideabi-)
	CFLAGS += -fPIE
	LDFLAGS += -pie
endif
ifeq ($(CROSS_COMPILE), mipsel-openwrt-linux-)
	CFLAGS += -std=gnu99 
	LDFLAGS += -ldl
	#LDFLAGS += -pie
	HAVE_LIBEV = 0
	DEBUG = 0
	CFLAGS += -DSTACK_SIZE=524288 
endif

DEBUG ?= 1
ifeq ($(DEBUG), 0)
	LDFLAGS +=-lm -s
	CFLAGS += -O2 -DNDEBUG
else
	LDFLAGS +=-lm
	CFLAGS += -g -DDEBUG
endif

HAVE_LIBEV ?= 1
ifneq ($(HAVE_LIBEV), 0)
	CFLAGS += -DHAVE_LIBEV
	LDFLAGS +=-lev
endif
HAVE_OPENSSL ?= 1
ifneq ($(HAVE_OPENSSL), 0)
	CFLAGS += -DHAVE_OPENSSL
	LDFLAGS +=-lssl -lcrypto
endif

CC = $(CROSS_COMPILE)gcc
#CC = $(CROSS_COMPILE)clang
LD = $(CROSS_COMPILE)ld
AS = $(CROSS_COMPILE)as
CPP = $(CC) -E
AR = $(CROSS_COMPILE)ar
NM = $(CROSS_COMPILE)nm
STRIP = $(CROSS_COMPILE)strip
OBJCOPY = $(CROSS_COMPILE)objcopy
OBJDUMP = $(CROSS_COMPILE)objdump

#INCLUDE_PATH = include
#CFLAGS += -I$(INCLUDE_PATH) -std=gnu99


export CC LD AS CPP AR NM STRIP OBJCOPY OBJDUMP CFLAGS LDFLAGS

ngrok:$(OBJ)
	$(CC) $^ -o $@ $(LDFLAGS) 


.PHONY:clean
clean:
	rm -rf *.o ngrok 



