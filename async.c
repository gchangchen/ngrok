
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <fcntl.h>
#include <errno.h>
#include <arpa/inet.h>
#include <assert.h>

#include "async.h"

#ifdef HAVE_LIBEV
#include <ev.h>
#else
#define EV_STANDALONE 1
#define EV_FEATURES 0
#define EV_MULTIPLICITY 1
#define EV_USE_EPOLL 1
//#define EV_API_STATIC 1
#include "libev/ev.c"
#endif //HAVE_LIBEV

#define HAVE_SETJMP_H 1
#define HAVE_SIGALTSTACK 1
#define CORO_STACKALLOC 0
#include "libcoro/coro.c"

#ifndef STACK_SIZE
#define STACK_SIZE (1024*1024)
#endif

typedef void (*coroutine_func)(void *ud);
struct schedule {
	char stack[STACK_SIZE];
	struct coro_context main_ctx;
	struct coroutine * volatile running;
	struct coroutine * volatile ready_co;
	struct coroutine * volatile waiting_co;
};
struct coroutine {
	struct coroutine *next;
	struct coro_context ctx;
	coroutine_func func;
	void *ud;
	ptrdiff_t cap;
	ptrdiff_t size;
	char *volatile stack;
	int events;
	void *data;
	ev_io io;
	ev_timer to;
};
static struct schedule *S= NULL;
static void **coroutine_data(struct coroutine *co){
	assert(co);
	return & co->data;
}
static void mainfunc(struct coroutine * volatile co){
	assert(co);
	S->running = co;
	co->func(co->ud);
	free(co->stack);
	co->stack = NULL;
	S->running = NULL;
	coro_transfer (& co->ctx, &S->main_ctx);
}
static struct coroutine *coroutine_new(coroutine_func func, void *ud) {
	if(S == NULL){
		S = malloc(sizeof(*S));
		if( S == NULL)return NULL;
		S->running = NULL;
		S->ready_co = NULL;
		S->waiting_co = NULL;
	}
	struct coroutine *co = malloc(sizeof(*co));
	if(co == NULL)return NULL;
	co->func = func;
	co->ud = ud;
	co->cap = 8 * 1024;
	co->size = 0;
	co->data = NULL;
	co->stack = malloc(co->cap);
	if(co->stack){
		co->next = S->ready_co;
		S->ready_co = co;
		return co;
	}
	free(co);
	return NULL;
}
static void coroutine_resume(struct coroutine *co){
	assert(S->running == NULL);
	assert(co);
	if(co->size){
		//恢复协程的栈。
		memcpy(S->stack + STACK_SIZE - co->size, co->stack, co->size);
	}else{
		coro_create(& co->ctx, (void (*)(void*))mainfunc, (void *)co, S->stack, STACK_SIZE);
	}

	S->running = co;
	//printf("transfer to %p\n", co);
	coro_transfer(& S->main_ctx, &co->ctx);
	S->running = NULL;
	if(co->stack == NULL){
		coro_destroy (& co->ctx);
		free(co);
	}else{
		//保存协程的栈内容，在这里保存是为了防止丢失切换过程中的栈内容。
		memcpy(co->stack, S->stack + STACK_SIZE - co->size, co->size);
	}
}
static void coroutine_yield(void) {
	struct coroutine *co = S->running;
	assert(co);
	assert((char *)&co > S->stack);
	co->size = S->stack + STACK_SIZE - (char*)&co + sizeof(char*) * 4;
	if(co->cap < co->size){
		free(co->stack);
		co->cap = co->size;
		co->stack = malloc(co->cap);
		assert(co->stack);
	}
	co->next = S->waiting_co;
	S->waiting_co = co;
	S->running = NULL;
	coro_transfer(&co->ctx , &S->main_ctx);
}
static struct coroutine *coroutine_running(void) {
	return S->running;
}

#define BUF_SIZE	8192

#define DNS_HOST  0x01
#define DNS_HOST_V6  0x1c
#define DNS_SVR "127.0.0.1"
static struct sockaddr *dns_addr[4] = {0};

static struct ev_loop *loop;
SSL_CTX *default_ctx;
const struct sockaddr **async_init(const char *name_server_v4, const char *name_server_v6){
	loop = EV_DEFAULT;

	const char *name_server[2] = {name_server_v4, name_server_v6};
	if(name_server[0] == NULL)name_server[0]= DNS_SVR;
	for(int i=0; i<2 && name_server[i]; i++){
		char buf[512];
		strcpy(buf, name_server[i]);
		char *p = strchr(buf, ' ');
		if(p){
			*p = '\0';
			p++;
		}
		char *host[2] = {buf, p};
		for(int j=0; j<2 && host[j]; j++){
			p = strchr(buf, '#');
			if(p){
				*p = '\0';
				p++;
			}else{
				p = "53";
			}

			struct addrinfo *answer, hint;
			memset(&hint, 0, sizeof(hint));
			hint.ai_family = AF_UNSPEC;
			hint.ai_socktype = SOCK_DGRAM;
			int ret = getaddrinfo(host[j], p, &hint, &answer);
			if (ret != 0 || answer == NULL) {
				return NULL;
			}
			dns_addr[i*2+j] = malloc(answer->ai_addrlen);
			if(dns_addr[i*2+j] == NULL)return NULL;
			memcpy(dns_addr[i*2+j], answer->ai_addr, answer->ai_addrlen);
			freeaddrinfo(answer);
		}
	}

#ifdef HAVE_OPENSSL
    /* SSL 库初始化*/
    SSL_library_init();
    OpenSSL_add_all_algorithms();
    SSL_load_error_strings();
	default_ctx = SSL_CTX_new(SSLv23_method());
#endif // HAVE_OPENSSL

	return (const struct sockaddr**)dns_addr;
}
void async_destroy(void ){
	for(int i=0; i<sizeof(dns_addr)/sizeof(dns_addr[0]); i++){
		free(dns_addr[i]);
	}
	if(S){
		free(S);
		S = NULL;
	}
	ev_loop_destroy(EV_A);
	loop = NULL;

#ifdef HAVE_OPENSSL
	if(default_ctx){
		SSL_CTX_free(default_ctx);
		default_ctx = NULL;
	}
#endif // HAVE_OPENSSL
}
void async_run(){
	int need_continue = 1;
	while(S->ready_co || need_continue){
		while(S->ready_co){
			struct coroutine *co = S->ready_co;
			S->ready_co = co->next;
			coroutine_resume(co);
		}
		need_continue = ev_run(EV_A_ EVRUN_ONCE);
	}
	return;
}

struct coroutine* async_start(void (*fn)(void *), void *arg){
	return coroutine_new(fn, arg);
}
void coroutine_event_ready(int revents, struct coroutine *co){
	struct coroutine *waiting_co = S->waiting_co;
	struct coroutine *co_prev = waiting_co;
	if(waiting_co == co){
		S->waiting_co = waiting_co->next;
	}else{
		while(co_prev && co_prev->next != co){
			co_prev = co_prev->next;
		}
		if(co_prev){
			co_prev->next = co->next;
		}
	}
	if(co_prev){
		co->next = S->ready_co;
		S->ready_co = co;
	}else{
		co_prev = S->ready_co;
		while(co_prev && co_prev != co){
			co_prev = co_prev->next;
		}
		if(co_prev == NULL){
			return;
		}
	}
	co->events |= revents;
}

static void wait_cb_io (EV_P_ ev_io *w, int revents) {
	struct coroutine *co = (struct coroutine *)(((char *)w) - offsetof (struct coroutine, io));
	coroutine_event_ready(revents | ev_clear_pending (EV_A_ &co->to), co);
}
static void wait_cb_to (EV_P_ ev_timer *w, int revents) {
	struct coroutine *co = (struct coroutine *)(((char *)w) - offsetof (struct coroutine, to));
	coroutine_event_ready(revents | ev_clear_pending (EV_A_ &co->io), co);
}

int async_wait_wto(int fd, int events, ev_tstamp timeout){
	struct coroutine *co = coroutine_running();
	assert(co);
	//ev_once(EV_A_ fd, events, timeout, (void (*)(int,void*))coroutine_event_ready, co);
	ev_init (&co->io, wait_cb_io);
	if (fd >= 0) {
		ev_io_set (&co->io, fd, events);
		ev_io_start (EV_A_ &co->io);
	}
	ev_init (&co->to, wait_cb_to);
	if (timeout >= 0.) {
		ev_timer_set (&co->to, timeout, 0.);
		ev_timer_start (EV_A_ &co->to);
	}

	co->events = 0;
	coroutine_yield();
	ev_io_stop    (EV_A_ &co->io);
	ev_timer_stop (EV_A_ &co->to);

	if(co->events & EV_TIMER){
		errno = ETIMEDOUT;
		return -1;
	}
	if(co->events & (~(events | EV_TIMER))){
		return -1;
	}
	return 0;
}

int async_accept_wto(int fd, struct sockaddr *addr, socklen_t *addrlen, ev_tstamp timeout){
	setnonblocking(fd);
	async_wait_wto(fd, EV_READ, timeout);
	int newfd = accept(fd, addr, addrlen);
	if (newfd != -1) {
		setnonblocking(newfd);
	}
	return newfd;
}

void _async_accept_handle(void *arg){
	struct coroutine *co = coroutine_running();
	int fd = * coroutine_data(co) - NULL;
	void (*fn)(void *) = arg;
	int newfd;
	while(1){
		newfd = async_accept_wto(fd, NULL, NULL, -1);
		if(newfd == -1 && errno != EAGAIN && errno != EINTR)break;
		if(newfd != -1){
			async_start(fn, NULL + newfd);
		}
	}
}
struct coroutine* async_accept_handle(int fd, void (*fn)(int fd)){
	struct coroutine *co = async_start(_async_accept_handle, (void *)fn);
	* coroutine_data(co) = NULL + fd;
	return co;
}

int async_connect_wto(int fd, const struct sockaddr *addr, socklen_t addrlen, ev_tstamp timeout){
	setnonblocking(fd);
	int ret = connect(fd, addr, addrlen);
	if(ret == 0)return ret;
	if(ret == -1 && errno != EINPROGRESS)return -1;

	if(async_wait_wto(fd, EV_WRITE, timeout) == -1)return -1;

	addrlen = sizeof(ret);
	if(-1 == getsockopt(fd, SOL_SOCKET, SO_ERROR, (void *)&ret, &addrlen) || 0 != ret ) {
		return -1;
	}
	return 0;
}
int async_getipbyname(const char *host, unsigned short sa_family, void *result, size_t result_length){
	if(sa_family != AF_INET6)sa_family = AF_INET;
	int max_count = result_length / (sa_family == AF_INET ? sizeof(struct in_addr) : sizeof(struct in6_addr));
	if(host == NULL || result == NULL || max_count <= 0)return -1;
	unsigned char flag = *(unsigned char*)result;
	if(flag)flag = 1;
	memset(result, 0, result_length);

	if(inet_pton(sa_family, host, result) == 1){
		return 1;
	}

	size_t host_len = strlen(host);
	if(host_len < 3){
		return -1;
	}
	unsigned char buf[513];
	memcpy(buf, "\xff\x00\x01\x00\x00\x01\x00\x00\x00\x00\x00\x00", 12); 
	unsigned char *p = buf + 12;
	for(int i = 0; i<=host_len; i++){
		if(*(host+i) == '.' || i == host_len){
			*p = (unsigned char)(i -(p - buf -12));
			p += *p + 1;
		}else{
			buf[13 + i] = *(host+i);
		}
	}
	memcpy(buf+12+1+host_len, "\x00\x00\x01\x00\x01", 5);
	if(sa_family == AF_INET6){
		buf[host_len+15] = 0x1c;
	}

	int ip_count = 0;
	struct sockaddr *current_dns_addr = sa_family == AF_INET ? dns_addr[flag] : dns_addr[2+flag];
	if(current_dns_addr == NULL){
		if(sa_family == AF_INET6 && dns_addr[2]){
			current_dns_addr = dns_addr[2];
		}else{
			current_dns_addr = dns_addr[0];
		}
	}

	int dns_fd = socket(current_dns_addr->sa_family, SOCK_DGRAM, 0);
	if(dns_fd < 0){
		return -1;
	}
	setnonblocking(dns_fd);
	
	ssize_t n = 0;
	for(int i=0; i<2; i++){
		n = async_sendto(dns_fd, buf, host_len + 18, 0, current_dns_addr, current_dns_addr->sa_family == AF_INET
				? sizeof(struct sockaddr_in): sizeof(struct sockaddr_in6));
		if(n != host_len + 18)continue;
		n = async_recvfrom_wto(dns_fd, buf, sizeof(buf), 0, NULL, NULL, 0.4);
		if(n > 0)break;
	}
	if(n > 18){
		buf[n] = '\0';
		int querys = ntohs(*((unsigned short*)(buf+4)));
		int answers = ntohs(*((unsigned short*)(buf+6)));
		const char *p = (const char*)buf + 12;
		for(int i= 0 ; i < querys ; i ++){
			if(*p & 0xc0)p+=6;
			else p += strlen(p)+5;
			if((unsigned char*)p > buf + n)goto end;
		}
		for(int i = 0 ; i < answers ; i ++){
			if((unsigned char*)p > buf + n)break;
			if(*p & 0xc0)p+=2;
			else p += strlen(p)+1;
			if((unsigned char*)p > buf + n - 10)break;
			short type = ntohs(*((unsigned short*)p));
			short datalen = ntohs(*((unsigned short*)(p+8)));
			if((sa_family == AF_INET6 && type == DNS_HOST_V6 && datalen == 16) ||
					(sa_family == AF_INET && type == DNS_HOST && datalen == 4)){
				memcpy(result + datalen * ip_count, p+10, datalen);
				ip_count++;
				if(ip_count >= max_count)break;
			}
			p += datalen + 10;
		}
	}

end:
	if(dns_fd >= 0){
		close(dns_fd);
		dns_fd = -1;
	}
	return ip_count;
}

//flag = IPV4_FIRST(0), IPV6_FIRST(1), IPV4_ONLY(2), IPV6_ONLY(3)...
int async_connect_host_wto(const char *host, unsigned short port, ev_tstamp timeout, int flag){
	if(host == NULL || port == 0)return -1;

	int fd = -1;
	int ip_count;
	flag &= 0x03;

	if(flag == 1 || flag == 3){
		struct in6_addr v6_addr[16]={0};
		for(int j=0; j<2; j++){
			ip_count = async_getipbyname(host, AF_INET6, v6_addr, sizeof(v6_addr));
			for(int i=0; i<ip_count; i++){
				struct sockaddr_in6 addr6 = {0};
				addr6.sin6_family = AF_INET6;
				addr6.sin6_port = htons(port);
				addr6.sin6_addr = v6_addr[i];
				fd = socket(AF_INET6, SOCK_STREAM, 0);
				int ret = async_connect_wto(fd, (struct sockaddr *)&addr6, sizeof(addr6), timeout);
				if(ret < 0){
					close(fd);
					fd = -2;
				}else{
					return fd;
				}
			}
			memset(&v6_addr, 0x01, 1);
		}
		if(flag == 1){
			return async_connect_host_wto(host, port, timeout, 2);
		}
	}
	if(flag == 0 || flag == 2){
		struct in_addr v4_addr[16]={0};
		for(int j=0; j<2; j++){
			ip_count = async_getipbyname(host, AF_INET, v4_addr, sizeof(v4_addr));
			for(int i=0; i<ip_count; i++){
				struct sockaddr_in addr = {0};
				addr.sin_family = AF_INET;
				addr.sin_port = htons(port);
				addr.sin_addr = v4_addr[i];
				fd = socket(AF_INET, SOCK_STREAM, 0);
				int ret = async_connect_wto(fd, (struct sockaddr *)&addr, sizeof(addr), timeout);
				if(ret < 0){
					close(fd);
					fd = -2;
				}else{
					return fd;
				}
			}
			memset(&v4_addr, 0x01, 1);
		}
		if(flag == 0){
			return async_connect_host_wto(host, port, timeout, 3);
		}
	}
	return fd;
}

ssize_t async_read_wto(int fd, void *buf, size_t count, ev_tstamp timeout){
	assert(count > 0);
	ssize_t ret = -1;
	do{
		if(async_wait_wto(fd, EV_READ, timeout) == -1)break;

		ret = read(fd, buf, count);
	}while(ret == -1 && errno == EINPROGRESS);
	return ret;
}
ssize_t async_read_all_wto(int fd, void *buf, size_t count, ev_tstamp timeout){
	ssize_t ret = 0;
	while(ret < count){
		int n = async_read_wto(fd, buf + ret, count - ret, timeout);
		if(n <=0)break;
		ret += n;
	}
	return ret;
}

ssize_t async_write_wto(int fd, const void *buf, size_t count, ev_tstamp timeout){
	assert(count > 0);
	ssize_t ret = -1;
	do{
		if(async_wait_wto(fd, EV_WRITE, timeout) == -1)break;

		ret = write(fd, buf, count);
	}while(ret == -1 && errno == EINPROGRESS);
	return ret;
}
ssize_t async_write_all_wto(int fd, const void *buf, size_t count, ev_tstamp timeout){
	ssize_t ret = 0;
	while(ret < count){
		int n = async_write_wto(fd, buf + ret, count - ret, timeout);
		if(n <=0)break;
		ret += n;
	}
	return ret;
}

ssize_t async_sendto_wto(int fd, const void *buf, size_t len, int flags, const
		struct sockaddr *dest_addr, socklen_t addrlen, ev_tstamp timeout){
	if(async_wait_wto(fd, EV_WRITE, timeout) != 0)return -1;
	return sendto(fd, buf, len, flags, dest_addr, addrlen);
}
ssize_t async_recvfrom_wto(int fd, void *buf, size_t len, int flags,
		struct sockaddr *src_addr, socklen_t *addrlen, ev_tstamp timeout){
	if(async_wait_wto(fd, EV_READ, timeout) != 0)return -1;
	return recvfrom(fd, buf, len, flags, src_addr, addrlen);
}

int create_server(const char *addr, unsigned short port, int type){
	struct addrinfo hints;
	struct addrinfo *result, *rp;
	int s, listen_sock;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = type;
	//hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	char buf[8];
	sprintf(buf, "%d", port);
	s = getaddrinfo(addr, buf, &hints, &result);
	if (s != 0) {
//		LOGI("getaddrinfo: %s", gai_strerror(s));
		return -1;
	}

	for (rp = result; rp != NULL; rp = rp->ai_next) {
		listen_sock = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
		if (listen_sock == -1) {
			continue;
		}

		int opt = 1;
		setsockopt(listen_sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

		s = bind(listen_sock, rp->ai_addr, rp->ai_addrlen);
		if (s == 0) {
			break;
		} else {
//			LOGE("bind");
			close(listen_sock);
			listen_sock = -1;
		}
	}

	if (rp == NULL) {
//		LOGE("Could not bind");
		return -1;
	}

	freeaddrinfo(result);
	if(type == SOCK_STREAM && listen_sock >= 0){
		s = listen(listen_sock, SOMAXCONN);
		if(s == -1){
			close(listen_sock);
			listen_sock = -1;
		}
	}
	return listen_sock;
}
int setnonblocking(int fd){
	int flags;
	if (-1 == (flags = fcntl(fd, F_GETFL, 0))) {
		flags = 0;
	}
	return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
}

size_t async_forword(int src, int dst, ssize_t len, ev_tstamp timeout){
	size_t ret = 0;
	char *buf = malloc(BUF_SIZE);
	if(buf == NULL)return 0;
	while(len < 0 || ret < len){
		ssize_t n = async_read_wto(src, buf, len -ret > BUF_SIZE ? BUF_SIZE : len-ret, timeout);
		if(n <= 0)break;
		ssize_t m = async_write_all_wto(dst, buf, n, timeout);
		if(m > 0)ret += m;
		if(m <= 0 || m != n)break;
	}
	free(buf);
	return ret;
}

struct relay_ctx{
	int src;
	int dst;
	struct coroutine *co;
#ifdef HAVE_OPENSSL
	SSL *src_ssl;
	SSL *dst_ssl;
#endif // HAVE_OPENSSL
};
static void _async_relay(void *arg){
	struct relay_ctx *ctx = (struct relay_ctx *)arg;
	int src = ctx->src;
	int dst = ctx->dst;

	char *buf = malloc(BUF_SIZE);
	if(buf == NULL)return ;
	while(1){
		ssize_t n;
#ifdef HAVE_OPENSSL
		if(ctx->src_ssl){
			n = async_ssl_read_wto(ctx->src_ssl, buf, BUF_SIZE, -1);
		}else{
			n = async_read_wto(src, buf, BUF_SIZE, -1);
		}
#else
		n = async_read_wto(src, buf, BUF_SIZE, -1);
#endif // HAVE_OPENSSL
		if(n <= 0){
			break;
		}
		if(ev_is_active(EV_A_ &ctx->co->to)){
			ev_timer_again(EV_A_ &ctx->co->to);
		}
		ssize_t m = 0;
		while(m < n){
			int ret;
#ifdef HAVE_OPENSSL
			if(ctx->dst_ssl){
				ret  = async_ssl_write_wto(ctx->dst_ssl, buf + m, n - m, -1);
			}else{
				ret  = async_write_wto(dst, buf + m, n - m, -1);
			}
#else
			ret  = async_write_wto(dst, buf + m, n - m, -1);
#endif // HAVE_OPENSSL
			if(ret <=0){
				break;
			}
			m += ret;
			if(ev_is_active(EV_A_ &ctx->co->to)){
				ev_timer_again(EV_A_ &ctx->co->to);
			}
			//coroutine_event_ready(EV_WRITE, ctx->co);
		}
		if(m != n)break;
	}
	free(buf);
	coroutine_event_ready(EV_TIMER, ctx->co);
	ctx->co = NULL;

	//printf("coroutine %d -> %d exit\n", src, dst);
}
static void do_relay(struct relay_ctx *ctx0, ev_tstamp timeout){
	struct relay_ctx *ctx1= malloc(sizeof(*ctx1));
	if(ctx1 == NULL){
		goto end;
	}
	struct coroutine *co = coroutine_running();
	ctx0->co = co;
	ctx1->src = ctx0->dst;
	ctx1->dst = ctx0->src;
	ctx1->co = co;
#ifdef HAVE_OPENSSL
	ctx1->src_ssl = ctx0->dst_ssl;
	ctx1->dst_ssl = ctx0->src_ssl;
#endif // HAVE_OPENSSL

	//printf("relay start %d <-> %d \n", ctx0->src, ctx0->dst);
	struct coroutine *co0 = async_start(_async_relay, ctx0);
	struct coroutine *co1 = async_start(_async_relay, ctx1);
	do{
		async_sleep(timeout);
	}while((co->events & EV_TIMER) == 0);

	//printf("coroutine %d <-> %d timeout\n", ctx0->src, ctx0->dst);

	while(ctx0->co || ctx1->co){
		if(ctx0->co)coroutine_event_ready(EV_TIMER, co0);
		if(ctx1->co)coroutine_event_ready(EV_TIMER, co1);
		async_sleep(1);
	}
	//printf("coroutine %d <-> %d exit\n", ctx0->src, ctx0->dst);

end:
	free(ctx1);
	return ;
}
void async_relay(int fd0, int fd1, ev_tstamp timeout){
	struct relay_ctx *ctx= malloc(sizeof(*ctx));
	if(ctx){
		memset(ctx, 0, sizeof(*ctx));
		ctx->src = fd0;
		ctx->dst = fd1;
		do_relay(ctx, timeout);
		free(ctx);
	}
	return ;
}

#ifdef HAVE_OPENSSL
int async_ssl_do_handshake_wto(SSL *ssl, double timeout){
	int ret;
	int fd = SSL_get_fd(ssl);
	while( 1 != ( ret=SSL_do_handshake(ssl))){
		int err = SSL_get_error(ssl, ret);
		if(err == SSL_ERROR_WANT_WRITE){
			if(async_wait_wto(fd, EV_WRITE, timeout) == -1)
				break;
		}else if(err == SSL_ERROR_WANT_READ){
			if(async_wait_wto(fd, EV_READ, timeout) == -1)
				break;
		}else{
			ERR_print_errors_fp(stdout);
			break;
		}
	}
	return ret;
}
ssize_t async_ssl_write_wto(SSL *ssl, const void *buf, size_t count, double timeout){
	assert(count > 0);
	assert(ssl);
	int ret;
	int fd = SSL_get_fd(ssl);
	while( 0 >= ( ret=SSL_write(ssl, buf, count))){
		int err = SSL_get_error(ssl, ret);
		if(err == SSL_ERROR_WANT_WRITE){
			if(async_wait_wto(fd, EV_WRITE, timeout) == -1)
				break;
		}else if(err == SSL_ERROR_WANT_READ){
			if(async_wait_wto(fd, EV_READ, timeout) == -1)
				break;
		}else{
			ERR_print_errors_fp(stdout);
			break;
		}
	}
	return ret;
}
ssize_t async_ssl_read_wto(SSL *ssl, void *buf, size_t count, double timeout){
	assert(count > 0);
	assert(ssl);
	int ret;
	int fd = SSL_get_fd(ssl);
	while( 0 >= ( ret=SSL_read(ssl, buf, count))){
		int err = SSL_get_error(ssl, ret);
		if(err == SSL_ERROR_WANT_WRITE){
			if(async_wait_wto(fd, EV_WRITE, timeout) == -1)
				break;
		}else if(err == SSL_ERROR_WANT_READ){
			if(async_wait_wto(fd, EV_READ, timeout) == -1)
				break;
		}else{
			ERR_print_errors_fp(stdout);
			break;
		}
	}
	if(ret <= 0){
		ERR_print_errors_fp(stdout);
	}
	return ret;
}
ssize_t async_ssl_read_all_wto(SSL *ssl, void *buf, size_t count, ev_tstamp timeout){
	ssize_t ret = 0;
	while(ret < count){
		int n = async_ssl_read_wto(ssl, buf + ret, count - ret, timeout);
		if(n <=0)break;
		ret += n;
	}
	return ret;
}
ssize_t async_ssl_write_all_wto(SSL *ssl, const void *buf, size_t count, ev_tstamp timeout){
	ssize_t ret = 0;
	while(ret < count){
		int n = async_ssl_write_wto(ssl, buf + ret, count - ret, timeout);
		if(n <=0)break;
		ret += n;
	}
	return ret;
}

void async_fd_ssl_relay(int fd, SSL *ssl, double timeout){
	struct relay_ctx *ctx= malloc(sizeof(*ctx));
	if(ctx){
		memset(ctx, 0, sizeof(*ctx));
		ctx->src = fd;
		ctx->dst = SSL_get_fd(ssl);
		ctx->dst_ssl = ssl;
		do_relay(ctx, timeout);
		free(ctx);
	}
	return ;
}
void async_ssl_relay(SSL *ssl1, SSL *ssl2, double timeout){
	struct relay_ctx *ctx= malloc(sizeof(*ctx));
	if(ctx){
		memset(ctx, 0, sizeof(*ctx));
		ctx->src = SSL_get_fd(ssl1);
		ctx->dst = SSL_get_fd(ssl2);
		ctx->src_ssl = ssl1;
		ctx->dst_ssl = ssl2;
		do_relay(ctx, timeout);
		free(ctx);
	}
	return ;
}

//flag = IPV4_FIRST(0), IPV6_FIRST(1), IPV4_ONLY(2), IPV6_ONLY(3)...
SSL* async_ssl_connect_host_wto(const char *host, unsigned short port, double timeout, int flag){
	if(default_ctx == NULL && (default_ctx = SSL_CTX_new(SSLv23_method())) == NULL){
		return NULL;
	}
	SSL *ssl;
	if((ssl = SSL_new(default_ctx)) == NULL){
		//ERR_print_errors_fp(stdout);
		return NULL;
	}
	int fd = async_connect_host_wto(host, port, timeout, flag);
	if(fd >= 0){
		SSL_set_fd(ssl, fd);
		SSL_set_connect_state(ssl);
		if(1 != async_ssl_do_handshake_wto(ssl, timeout)){
			close(fd);
			fd = -1;
		}
	}
	if(fd < 0){
		SSL_free(ssl);
		ssl = NULL;
	}
	return ssl;
}
SSL *async_ssl_wrap_server_wto(int fd, SSL_CTX *ctx, const char *pem_file, double timeout){
	if(fd < 0 || pem_file == NULL)return NULL;
	//if(timeout < 0)timeout = 3;
	if(ctx == NULL)ctx = default_ctx;
	SSL *ssl = NULL;
	if (SSL_CTX_use_certificate_file(ctx, pem_file, SSL_FILETYPE_PEM) <= 0 || 
			SSL_CTX_use_PrivateKey_file(ctx, pem_file, SSL_FILETYPE_PEM) <= 0 ||
			(!SSL_CTX_check_private_key(ctx)) || (ssl = SSL_new(ctx)) == NULL) {
		//ERR_print_errors_fp(stdout);
		return NULL;
	}
	SSL_set_fd(ssl, fd);
	SSL_set_accept_state(ssl);
	if(1 != async_ssl_do_handshake_wto(ssl, timeout)){
		SSL_free(ssl);
		ssl = NULL;
	}
	return ssl;
}

#endif // HAVE_OPENSSL
