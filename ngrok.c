#include <signal.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <net/if.h>
#include <assert.h>
#include <stddef.h>
#include <limits.h>
#include <linux/netfilter_ipv4.h>
#include <linux/netfilter_ipv6/ip6_tables.h>

#ifndef HAVE_OPENSSL
#define HAVE_OPENSSL
#endif
#include "async.h"
#include "parson.h"
#include "utils.h"

static ssize_t read_pack(SSL *ssl, unsigned char *buf, size_t buf_len, double timeout){
	if(ssl == NULL || buf == NULL || buf_len < 9)return -1;
	if ( 8 > async_ssl_read_all_wto(ssl, buf, 8, timeout)){
		return -1;
	}
	size_t body_len = buf[0] | (buf[1] << 8) | (buf[2] << 16) | (buf[3] << 24) ;
	if(body_len > buf_len -1 || body_len != async_ssl_read_all_wto(ssl, buf, body_len, timeout)){
		return -1;
	}
	buf[body_len] = '\0';
	LOGD("<---:%s", buf);
	return body_len;
}
static ssize_t write_pack(SSL *ssl, char *buf, double timeout){
	if(ssl == NULL || buf == NULL)return 0;
	size_t len = strlen(buf+8);
	buf[0] = len & 0xff;
	buf[1] = (len >> 8) & 0xff;
	buf[2] = (len >> 16) & 0xff;
	buf[3] = (len >> 24) & 0xff;
	memset(buf+4, 0, 4);
	LOGD("--->:%s", buf+8);
	if(len+8 != async_ssl_write_all_wto(ssl, buf, len+8, timeout)){
		return -1;
	}
	return len+8;
}

struct session_ctx{
	JSON_Array *tunnel_array;
	SSL *ssl;
};
static void ngrok_proxy(void *arg){
	LOGD("proxy session start");
	struct session_ctx *ctx = arg;
	assert(ctx);
	assert(ctx->tunnel_array);
	assert(ctx->ssl);

	SSL *ssl = ctx->ssl;
	JSON_Array *tunnel_array = ctx->tunnel_array;
	JSON_Value *root_value = NULL;
	if(tunnel_array){
		unsigned char buf[1024];
		if( read_pack(ssl, buf, sizeof(buf), -1) > 0){
			root_value = json_parse_string(buf);
		}
	}
	if(root_value){
		JSON_Object *root_object = json_value_get_object(root_value);
		const char *type = json_object_get_string(root_object, "Type");
		if(strcmp(type, "StartProxy") == 0){
			const char *url = json_object_dotget_string(root_object, "Payload.Url");
			for (int i = 0; i < json_array_get_count(tunnel_array); i++) {
				JSON_Object *tunnel = json_array_get_object(tunnel_array, i);
				if(strcmp(url, json_object_get_string(tunnel, "url")) == 0){
					LOGI("connect to %s:%d", json_object_get_string(tunnel, "lhost"),
							(unsigned short)json_object_get_number(tunnel, "lport"));
					int fd = async_connect_host_wto( json_object_get_string(tunnel, "lhost"),
							(unsigned short)json_object_get_number(tunnel, "lport"), 3, 0);
					if(fd < 0){
						LOGW("connect local ERROR!");
						unsigned char buf[1024];
						unsigned char body[1024];
						int len = sprintf(body, "<html><body style='background-color: #97a8b9'><div style='margin:auto; width:400px;padding: 20px 60px; background-color: #D3D3D3; border: 5px solid maroon;'><h2>Tunnel %s unavailable</h2><p>Unable to initiate connection to <strong>%s:%d</strong>. This port is not yet available for web server.</p>", url,
								json_object_get_string(tunnel, "lhost"), (unsigned short)json_object_get_number(tunnel, "lport"));
						len = sprintf(buf, "HTTP/1.0 502 Bad Gateway\r\nContent-Type: text/html\r\nContent-Length: %d\r\n\r\n%s",
								len, body);
						async_ssl_write_all_wto(ssl, buf, len, 3);
					}
					json_value_free(root_value);
					root_value = NULL;
					if(fd >= 0){
						async_fd_ssl_relay(fd, ssl, -1);
						LOGI("ngrok relay exited!");
						close(fd);
						fd = -1;
					}
					break;
				}
			}
		}else{
			LOGW("Unknown type:%s", type);
		}
		if(root_value){
			json_value_free(root_value);
			root_value = NULL;
		}
	}

	int fd = SSL_get_fd(ssl);
	SSL_free(ssl);
	close(fd);
	free(ctx);
}

static int main_quit = 0;
void set_main_quit(int dunno){
	main_quit = 1;
}
static void ngrok_client(void *arg){
	JSON_Object *config = arg;
	JSON_Array *tunnel_array = json_object_get_array(config, "client");
	unsigned char clientId[33] = {0};

	while(main_quit == 0){
		unsigned char buf[1024];
		SSL *ssl = async_ssl_connect_host_wto(json_object_dotget_string(config, "server.host"), 
				json_object_dotget_number(config, "server.port"), 3, 0);

		if(ssl){
			strcpy(buf, "AAAAAAAA{\"Type\":\"Auth\",\"Payload\":{\"Version\":\"2\",\"MmVersion\":\"1.7\",");
			if(json_object_dothas_value(config, "server.authtoken")){
				strcat(buf, "\"AuthToken\":\"");
				strcat(buf, json_object_dotget_string(config, "server.authtoken"));
				strcat(buf, "\",");
			}
			if(json_object_dothas_value(config, "server.user")){
				strcat(buf, "\"User\":\"");
				strcat(buf, json_object_dotget_string(config, "server.user"));
				strcat(buf, "\",");
			}
			if(json_object_dothas_value(config, "server.password")){
				strcat(buf, "\"Password\":\"");
				strcat(buf, json_object_dotget_string(config, "server.password"));
				strcat(buf, "\",");
			}
			strcat(buf, "\"OS\":\"darwin\",\"Arch\":\"amd64\",\"ClientId\":\"\"}}");
			if(write_pack(ssl, buf, 3) <= 0){
				LOGW("Send Auth error!");
				int fd = SSL_get_fd(ssl);
				SSL_free(ssl);
				close(fd);
				ssl = NULL;
			}
		}else{
			LOGE("connect to %s:%d error", json_object_dotget_string(config, "server.host"),
					(unsigned short)json_object_dotget_number(config, "server.port"));
			async_sleep(300);
			continue;
		}

		JSON_Value *root_value = NULL;
		while(ssl){
			ssize_t len = read_pack(ssl, buf, sizeof(buf), 25);
			if(len < 0){
				if(main_quit)break;
				//send ping 
				strcpy(buf, "AAAAAAAA{\"Type\":\"Ping\",\"Payload\":{}}");
				if(errno == ETIMEDOUT && write_pack(ssl, buf, 3) > 0){
					continue;
				}
				LOGW("Read pack error!");
				break;
			}
			//TODO check json format
			root_value = json_parse_string(buf);
			JSON_Object *root_object = json_value_get_object(root_value);
			const char *type = json_object_get_string(root_object, "Type");
			const char *error = json_object_dotget_string(root_object, "Payload.Error");
			if(error && strcmp(error, "") != 0){
				LOGE("ERROR: %s %s", type ? type : "" , error);
				break;
			}
			if(strcmp(type, "AuthResp") == 0){
				//auth ok, send first ping.
				//strcpy(buf, "AAAAAAAA{\"Type\":\"Ping\",\"Payload\":{}}");
				//if(write_pack(ssl, buf, 3) <= 0 )break;

				strncpy(clientId, json_object_dotget_string(root_object, "Payload.ClientId"), sizeof(clientId)-1);
				clientId[sizeof(clientId) -1 ] = '\0';
				LOGD("clientId:%s", clientId);
				const char chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
				for (int i = 0; i < json_array_get_count(tunnel_array); i++) {
					JSON_Object *tunnel = json_array_get_object(tunnel_array, i);
					char reqid[9]={0};
					for(int j=0;j<sizeof(reqid)-1;j++){
						reqid[j] = chars[rand() % (strlen(chars)-1)];
					}
					if(JSONSuccess != json_object_set_string(tunnel, "reqid", reqid)){
						break;
					}
					sprintf(buf, "AAAAAAAA{\"Type\": \"ReqTunnel\", \"Payload\": {\"ReqId\": \"%s\", \"Protocol\": \"%s\", \"Hostname\": \"%s\", \"Subdomain\": \"%s\", \"HttpAuth\": \"\", \"RemotePort\": %d, \"authtoken\":\"%s\"}}", reqid,
							json_object_get_string(tunnel, "protocol"),
							json_object_get_string(tunnel, "hostname"),
							json_object_get_string(tunnel, "subdomain"),
							(int)json_object_get_number(tunnel, "rport"),
							json_object_has_value(tunnel, "authtoken") ? json_object_get_string(tunnel, "authtoken"): "" );

					if(write_pack(ssl, buf, 3) <= 0){
						LOGW("write pack error!");
						break;
					}
				}
				strcpy(buf, "AAAAAAAA{\"Type\":\"Ping\",\"Payload\":{}}");
				if(write_pack(ssl, buf, 3) <= 0){
					LOGW("write pack error!");
					break;
				}
			}else if(strcmp(type, "ReqProxy") == 0){
				if(clientId[0] == '\0')break;
				SSL *proxy_ssl = async_ssl_connect_host_wto(json_object_dotget_string(config, "server.host"), 
						json_object_dotget_number(config, "server.port"), 3, 0);
				if(proxy_ssl == NULL){
					LOGW("proxy_ssl == NULL");
					continue;
				}
				sprintf(buf, "AAAAAAAA{\"Type\":\"RegProxy\",\"Payload\":{\"ClientId\":\"%s\"}}", clientId);
				if(write_pack(proxy_ssl, buf, 3) <= 0){
					LOGW("write_pack to proxy_ssl error!");
					break;
				}
				struct session_ctx *ctx = malloc(sizeof(*ctx));
				if(ctx == NULL){
					int fd = SSL_get_fd(proxy_ssl);
					SSL_free(proxy_ssl);
					close(fd);
					break;
				}
				ctx->ssl = proxy_ssl;
				ctx->tunnel_array = tunnel_array;
				async_start(ngrok_proxy, ctx);
			}else if(strcmp(type, "NewTunnel") == 0){
				const char *error = json_object_dotget_string(root_object, "Payload.Error");
				if(strcmp(error, "") == 0){
					const char *reqid = json_object_dotget_string(root_object, "Payload.ReqId");
					const char *url = json_object_dotget_string(root_object, "Payload.Url");
					for (int i = 0; i < json_array_get_count(tunnel_array); i++) {
						JSON_Object *tunnel = json_array_get_object(tunnel_array, i);
						if(strcmp(reqid, json_object_get_string(tunnel, "reqid")) == 0){
							if(JSONSuccess != json_object_set_string(tunnel, "url", url)){
								LOGE("Set Url to tunnel error!");
							}
							if(json_object_has_value(tunnel, "cmd")){
								const char *cmd = json_object_get_string(tunnel, "cmd");
								if(cmd && *cmd != '\0'){
									LOGI("cmd:%s %s", cmd, url);
									pid_t pid;
									if((pid=fork()) == 0){
										//child
										execlp(cmd, cmd, url, (char*)0);
										exit(0);
									}
								}
							}
							break;
						}
					}

				}else{
					LOGE("NewTunnel error:%s", error);
				}
			}else if(strcmp(type, "Pong") == 0){
			}else{
				LOGW("%s", buf);
			}
			if(root_value){
				json_value_free(root_value);
				root_value = NULL;
			}
		}
		if(root_value){
			json_value_free(root_value);
			root_value = NULL;
		}

		if(ssl){
			int fd = SSL_get_fd(ssl);
			SSL_free(ssl);
			close(fd);
			ssl = NULL;
		}
		for (int i = 0; i < json_array_get_count(tunnel_array); i++) {
			JSON_Object *tunnel = json_array_get_object(tunnel_array, i);
			json_object_remove(tunnel, "reqid");
			json_object_remove(tunnel, "url");
		}
	}
}

int main() {
	srand(time(NULL));
	async_init("114.114.114.114", NULL);

	JSON_Value *config_value = json_parse_file_with_comments("/etc/ngrok.json");
	if(config_value == NULL){
		config_value = json_parse_file_with_comments("ngrok.json");
	}
	if(config_value == NULL){
		LOGE("read config file error!");
		exit(1);
	}

	switch(json_value_get_type(config_value)){
		case JSONArray:
			{
				JSON_Array *config_array = json_value_get_array(config_value);
				for (int i = 0; i < json_array_get_count(config_array); i++) {
					async_start(ngrok_client, json_array_get_object(config_array, i));
				}
			}
			break;
		case JSONObject:
			async_start(ngrok_client, json_value_get_object(config_value));
			break;
		default:
			LOGE("parse config file error!");
			exit(1);

	}

	
	//daemonize("/var/run/ngrok.pid");
	signal(SIGPIPE, SIG_IGN);
	signal(SIGCHLD,SIG_IGN);
	signal(SIGUSR1, set_main_quit);
	LOG_LEVEL(LOG_INFO);
	// Enter the loop
	async_run();
	async_destroy();

	json_value_free(config_value);

	return 0;
}

