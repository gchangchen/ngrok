
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <pwd.h>
#include <time.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netdb.h>


#include "utils.h"


int _log_level = LOG_DEBUG;
int _use_syslog = 0;
void LOG_LEVEL(int level){
	_log_level = LOG_PRI(level);
}
void LOG_INIT(int use_syslog){
	if(use_syslog)
		_use_syslog = 1;
	openlog(NULL, LOG_CONS | LOG_PID, 0);
}
void LOG(int priority, const char *format, ...){
	if(LOG_PRI(priority) > _log_level)return ;
	va_list ap;
	va_start(ap, format);
	if (_use_syslog) {
		vsyslog(priority, format, ap);
	} else {
		time_t now = time(NULL);
		char time_str[20];
		strftime(time_str, 20, "%F %T", localtime(&now));
		const char *priority_str[]={"EMERG", "ALER", "CRIT",
			"ERR", "WARNING", "NOTICE", "INFO", "DEBUG"};
		assert(LOG_PRI(priority) < sizeof(priority_str)/sizeof(char*));
		char buf[8192];
		size_t n = 0;
		if(isatty(STDERR_FILENO)){
			n = sprintf(buf, "\e[01;35m %s %s: \e[0m", time_str,
					priority_str[LOG_PRI(priority)]);
		}else{
			n = sprintf(buf, " %s %s: ", time_str,
					priority_str[LOG_PRI(priority)]);
		}
		vsprintf(buf + n, format, ap);
		strcat(buf, "\n");
//		printf("%d\t%s", strlen(buf), buf);
		fprintf(stderr, "%s", buf);
	}
	va_end(ap);
}

void daemonize(const char *path) {
    /* Our process ID and Session ID */
    pid_t pid, sid;

    /* Fork off the parent process */
    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    /* If we got a good PID, then
       we can exit the parent process. */
    if (pid > 0){
		if(path){
			FILE *file = fopen(path, "w");
			if (file == NULL) {
				FATAL("Invalid pid file\n");
			}

			fprintf(file, "%d", pid);
			fclose(file);
		}
        exit(EXIT_SUCCESS);
    }

    /* Change the file mode mask */
    umask(0);

    /* Open any logs here */

    /* Create a new SID for the child process */
    sid = setsid();
    if (sid < 0) {
        /* Log the failure */
        exit(EXIT_FAILURE);
    }

    /* Change the current working directory */
    if ((chdir("/")) < 0) {
        /* Log the failure */
        exit(EXIT_FAILURE);
    }

    /* Close out the standard file descriptors */
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

	LOG_INIT(1);
}

int run_as(const char *user) {
	if (user == NULL || user[0] == '\0')return -1;
	if(geteuid() != 0){
		LOGW("Run_as error: euid is not 0");
		return 1;
	}
	struct passwd *pwd;

	if (!(pwd = getpwnam(user))) {
		LOGE("run_as user %s could not be found.", user);
		return 0;
	}
	if (setgid(pwd->pw_gid) != 0 || setuid(pwd->pw_uid) != 0){
		LOGE("Could not run_as to user '%s': %s", user, strerror(errno));
		return 0;
	}
}


