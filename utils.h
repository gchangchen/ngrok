
#ifndef _UTILS_H
#define _UTILS_H

#include <syslog.h>
#include <stdlib.h>

#ifdef NDEBUG
//#define LOGD(format, ...)
#define LOGD(format, ...) LOG(LOG_DEBUG, format, ## __VA_ARGS__)
#define LOGE(format, ...) LOG(LOG_ERR, format, ## __VA_ARGS__)
#define LOGI(format, ...) LOG(LOG_INFO, format, ## __VA_ARGS__)
#define LOGW(format, ...) LOG(LOG_WARNING, format, ## __VA_ARGS__)
#else
#define LOGE(format, ...) LOG(LOG_ERR, "%s:%d "format, __FILE__, __LINE__, ## __VA_ARGS__)
#define LOGI(format, ...) LOG(LOG_INFO, "%s:%d "format, __FILE__, __LINE__, ## __VA_ARGS__)
#define LOGW(format, ...) LOG(LOG_WARNING, "%s:%d "format, __FILE__, __LINE__, ## __VA_ARGS__)
#define LOGD(format, ...) LOG(LOG_DEBUG, "%s:%d "format, __FILE__, __LINE__, ## __VA_ARGS__)
#endif

#define FATAL(msg)			do{ LOGE("%s", msg); exit(-1);}while(0)

void LOG_LEVEL(int level);
void LOG(int priority, const char *format, ...);
int run_as(const char *user);
void daemonize(const char * path);

#endif // _UTILS_H
